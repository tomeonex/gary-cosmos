﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour {

    public Image soundButtonImage;

    private AudioSource[] source;
    private Sprite[] sprite;
    

	void Start () {
        source = FindObjectsOfType<AudioSource>();

        foreach(AudioSource s in source)
        {
        s.volume = PlayerPrefs.GetInt("Sound",1);
        
        }

        sprite = Resources.LoadAll<Sprite>("Sprites/UI/sound");
        soundButtonImage.sprite = sprite[0];
        if (PlayerPrefs.GetInt("Sound") == 1)
        soundButtonImage.sprite = sprite[0];
        else soundButtonImage.sprite = sprite[1];
    }

    public void SetVolume()
    {
        if (PlayerPrefs.GetInt("Sound") == 1)
        {
            PlayerPrefs.SetInt("Sound", 0);
            soundButtonImage.sprite = sprite[1];
        }
        else
        {
            PlayerPrefs.SetInt("Sound", 1);
            soundButtonImage.sprite = sprite[0];
        }

        foreach (AudioSource s in source)
        {
            s.volume = PlayerPrefs.GetInt("Sound");
        }
    }
}
