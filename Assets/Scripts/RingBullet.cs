﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingBullet : MonoBehaviour {
    
	void Start () {
        gameObject.transform.localScale = new Vector3(0, 0, 0);
	}
	
	void Update () {
        gameObject.transform.localScale += new Vector3(1f * Time.deltaTime, 1f * Time.deltaTime, 0);
    }
}
