﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Boss2 : MonoBehaviour
{
    public int hp;
    public float timeBetweenShots = 2f;

    public LevelChanger levelChanger;

    private FlashSprite flashSprite;
    private Bullet bullet;
    
    void Start()
    {
        bullet = GetComponent<Bullet>();
        flashSprite = GetComponent<FlashSprite>();
        bullet.InvokeRepeating("ThreeBullets", .5f, 2f);
        bullet.InvokeRepeating("AimPlayer", 1f, timeBetweenShots);
    }

    void Update()
    {
        //Destroy boss
        if (hp <= 0)
        {
            Destroy(gameObject);
            levelChanger.FadeToLevel("Game");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "bulletPlayer")
        {
            flashSprite.StartCoroutine("FlashSpriteBlack");
            hp--;
            Destroy(collision.gameObject);
        }
    }

}
