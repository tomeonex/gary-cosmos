﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss6 : MonoBehaviour {

    public int hp;
    public float bulletSpeed;
    public float timeBetweenRing;
    public float timeBetweenRandom;
    public int numberRandom;
    public float timeBetweenShake;

    public LevelChanger levelChanger;
    public CameraShake cameraShake;
    
    private bool isAiming = true;
    private float timer = 0f;
    private float ringTimer = 0f;
    private float shakeTimer = 0f;

    private Animator anim;
    private Bullet bullet;
    private Rigidbody2D rig;
    private GameObject player;
    private FlashSprite flashSprite;

    void Start () {
        flashSprite = GetComponent<FlashSprite>();
        bullet = GetComponent<Bullet>();
        rig = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player");
    }
	
	void Update () {
        
        //Spam ring bullets
        ringTimer += Time.deltaTime;
        if(ringTimer >= timeBetweenRing && isAiming)
        {
            StartCoroutine("AimRingBullet");
            ringTimer = 0f;
        }

        //Random fire
        timer += Time.deltaTime;
        if(timer >= timeBetweenRandom)
        {
            bullet.RandomFire(numberRandom);
            timer = 0f;
        }

        //Camera shake
        shakeTimer += Time.deltaTime;
        if(shakeTimer >= timeBetweenShake)
        {
            StartCoroutine(cameraShake.Shake(1f, .1f));
            shakeTimer = 0;
        }
	}

    IEnumerator AimRingBullet()
    {
        RingBullet();
        yield return new WaitForSeconds(.1f);
        RingBullet();
        yield return new WaitForSeconds(.1f);
        RingBullet();
        yield return new WaitForSeconds(.1f);
        RingBullet();
        yield return new WaitForSeconds(.1f);

    }
    
    public void RingBullet()
    {
        bullet.RingBullet(new Vector3((player.transform.position.x - gameObject.transform.position.x),
        (player.transform.position.y - gameObject.transform.position.y),
        0), bulletSpeed);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "bulletPlayer")
        {
            flashSprite.StartCoroutine("FlashSpriteBlack");
            hp--;
            Destroy(collision.gameObject);
        }
    }
}
