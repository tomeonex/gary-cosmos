﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss5 : MonoBehaviour {

    public int hp;
    public float speed;
    public float speedAttack;
    public float timeBetweenShots;
    public float timeBetween5bullets;

    public LevelChanger levelChanger;

    private float padding = 0.5f;
    private float xmin;
    private float xmax;
    private float tempTime = 0f;
    private float attackTime = 0f;
    private float fiveTime = 0f;
    private int direction = 1;
    private bool isShooting = true;
    private bool isBack = false;
    private bool isAttacking = false;

    private Animator anim;
    private Bullet bullet;
    private Rigidbody2D rig;
    private FlashSprite flashSprite;


    void Start () {
        bullet = GetComponent<Bullet>();
        anim = GetComponentInChildren<Animator>();
        rig = GetComponent<Rigidbody2D>();
        flashSprite = GetComponent<FlashSprite>();

        float distance = transform.position.z - Camera.main.transform.position.z;
        Vector3 leftmost = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance));
        Vector3 rightmost = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, distance));
        xmin = leftmost.x + padding;
        xmax = rightmost.x - padding;
        
    }
	
	void Update () {

        if (isShooting)
        {
            transform.Translate(direction * speed * Time.deltaTime, 0, 0);
        }
        if (transform.position.x <= xmin || transform.position.x >= xmax) direction *= -1;

        //Destroy boss
        if (hp <= 0)
        {
            Destroy(gameObject);
            levelChanger.FadeToLevel("Game");
        }

        //Shoot
        tempTime += Time.deltaTime;
        if (tempTime > timeBetweenShots && isShooting)
        {
                bullet.AimPlayer();
                tempTime = 0;
        }

        //5 bullets
        fiveTime += Time.deltaTime;
        if(fiveTime > timeBetween5bullets && isShooting)
        {
            bullet.FiveBullets();
            fiveTime = 0f;
        }

        //attack
        attackTime += Time.deltaTime;
        if(attackTime >= 5f)
        {
            isAttacking = true;
            isShooting = false;
            anim.SetTrigger("attack");
            StartCoroutine("Attack");
            attackTime = 0f;
        }

        //back after attack
        if(transform.position.y <= -4)
        {
            rig.velocity = new Vector2(0, 1) * speedAttack;
            isBack = true;
        }

        //back to moving left and right
        if (transform.position.y > 3.5f && !isShooting && isBack)
        {
            isAttacking = false;
            rig.velocity = Vector2.zero;
            attackTime = 0f;
            isBack = false;
            isShooting = true;
        }
    }

    IEnumerator Attack()
    {
        yield return new WaitForSeconds(1.7f);
        rig.velocity = new Vector2(0, -1) * speedAttack;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "bulletPlayer")
        {
            flashSprite.StartCoroutine("FlashSpriteBlack");
            hp--;
            Destroy(collision.gameObject);
        }
    }
}
