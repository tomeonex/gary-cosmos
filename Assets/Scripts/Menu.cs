﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour {

    public Canvas CreditsCanvas;
    public Canvas MenuCanvas;
    public Canvas HighScore;
    public Text highScore;

    private int[] highScoreArray;

    private void Awake()
    {
        Screen.SetResolution(800, 1920, true);
    }

    void Start () {
        
        if (SceneManager.GetActiveScene().name.Equals("Menu"))
        {
            PlayerPrefs.SetInt("BulletsState", 0);
            PlayerPrefs.SetInt("Lifes", 3);
            PlayerPrefs.SetInt("Level", 1);
            PlayerPrefs.SetFloat("Shoot", 1f);
            PlayerPrefs.SetInt("Scores", 0);
            highScoreArray = new int[10];
            LoadHighScore();
        }
        
	}

    public void LoadScene()
    {
        SceneManager.LoadScene("Game");
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void Credits()
    {
        MenuCanvas.enabled = false;
        HighScore.enabled = false;
        CreditsCanvas.enabled = true;
    }

    public void HighScoreCanvas()
    {
        MenuCanvas.enabled = false;
        HighScore.enabled = true;
        CreditsCanvas.enabled = false;
    }

    public void Back()
    {
        CreditsCanvas.enabled = false;
        HighScore.enabled = false;
        MenuCanvas.enabled = true;
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("Menu");
    }
    
    public void LoadHighScore()
    {
        string highScoreList = "";
        for(int i = 0; i < 10; i++)
        {
            highScoreArray[i] = PlayerPrefs.GetInt("HighScore" + i);
        }
        for(int j = 9; j >= 0; j--)
        {
            if (highScoreArray[j] == 0) highScoreList += "\n";
            else highScoreList += highScoreArray[j] + "\n";
        }
        highScore.text = highScoreList;
    }
}
