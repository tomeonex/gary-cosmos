using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
    
    public CameraShake cameraShake;

    private float speed = 4.5f;
    private float padding = 0.5f;
    private float xmin;
    private float xmax;
    private float tempTime = 0;
    private float timeBetweenShots = 1f;
    private int lifes;
    private int bonusCounter = 0;
    private enum Bullets { one, two, three };

    private Bullet bullet;
    private Transform sprite;
    private Bullets bullets;
    private Flash flash;
    private AudioSource source;

    void Start()
    {

        lifes = 5;
        timeBetweenShots = PlayerPrefs.GetFloat("Shoot");
        bullet = GetComponent<Bullet>();
        sprite = gameObject.transform.GetChild(0);
        flash = GetComponent<Flash>();

        bullets = (Bullets)PlayerPrefs.GetInt("BulletsState");
        source = GetComponent<AudioSource>();

        float distance = transform.position.z - Camera.main.transform.position.z;
        Vector3 leftmost = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance));
        Vector3 rightmost = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, distance));
        xmin = leftmost.x + padding;
        xmax = rightmost.x - padding;

    }

    void Update()
    {
        tempTime += Time.deltaTime;
            if (tempTime > timeBetweenShots)
            {
                {
                    Fire();
                    tempTime = 0;
                }
            }

        if(bonusCounter % 3 == 0 && bonusCounter > 0)
        {
            bonusCounter++;
            if((int)bullets < 3)
            {
                bullets++;
            }
            
        }
    }

    void FixedUpdate () {
        
        if (Input.touchCount > 0)
        {
            Vector3 touchedPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            if (touchedPos.y < 3.5f)
            {
                float direction = Mathf.Clamp(touchedPos.x - transform.position.x, -1, 1);
                transform.Translate(direction * speed * Time.deltaTime, 0, 0);
                sprite.transform.rotation = Quaternion.Euler(0, direction * 30, 0);
            }
        }
        
#if UNITY_EDITOR
        float xx = Input.GetAxis("Horizontal");

        Vector3 movement2 = new Vector3(xx, 0, 0);
        transform.position += movement2 * 5f * Time.deltaTime;

        float newXX = Mathf.Clamp(transform.position.x, xmin, xmax);
        transform.position = new Vector3(newXX, transform.position.y, transform.position.z);
        sprite.transform.rotation = Quaternion.Euler(0, movement2.x * 30, 0);
#endif
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.transform.tag == "meteor" ||
            collision.transform.tag == "meteorGod")
        {
            Trigger();
        }

        if(collision.transform.tag == "bulletmeteor" || collision.transform.tag == "bulletboss" || collision.transform.tag == "bulletenemy")
        {
            Trigger();
            Destroy(collision.gameObject);
        }
        if (collision.tag == "bonus")
        {
            bonusCounter++;
            timeBetweenShots *= 0.9f;
        }

        if (collision.CompareTag("boss"))
        {
            Trigger();
        }
    }

    public int GetLifes()
    {
        return lifes;
    }

    public void IncreaseLifes()
    {
        lifes++;
    }

    public float GetTimeBetweenShoots()
    {
        return timeBetweenShots;
    }

    public int GetBullets()
    {
        return (int)bullets;
    }
    
    public void Fire()
    {
        switch (bullets)
        {
            case Bullets.one:
                bullet.Fire(new Vector3(0, 1, 0));
                break;
            case Bullets.two:
                bullet.FireDouble(new Vector3(0, 1, 0));
                break;
            case Bullets.three:
                bullet.FireTriple(new Vector3(0, 1, 0));
                break;
        }
    }

    public void Trigger()
    {
        source.Play();
        flash.FlashBang();
        StartCoroutine(cameraShake.Shake(.3f, .1f));
        lifes--;
    }
}
