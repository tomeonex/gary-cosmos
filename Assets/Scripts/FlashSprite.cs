﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashSprite : MonoBehaviour {

    public SpriteRenderer sprite;

    public IEnumerator FlashSpriteBlack()
    {
        sprite.color = Color.black;
        yield return new WaitForSeconds(.02f);
        sprite.color = Color.white;
    }
}
