﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    public LevelChanger levelChanger;
    public PlayerController playerController;
    public MeteorSpawn meteorSpawn;
    public EnemySpawn enemySpawn;
    public Text scoreText;
    public Text livesText;

    private int scores;
    private int level;
    private int playerLifes;
    private int[] highScore;
    
	void Start () {
        highScore = new int[10];
        scores = PlayerPrefs.GetInt("Scores");
        level = PlayerPrefs.GetInt("Level");
        LoadHighScore();
        
        if (SceneManager.GetActiveScene().name.Equals("Game"))
        {
            for (int i = 1; i < level; i++)
            {
                meteorSpawn.SpawnTimeDecreas();
            }
        }
	}
	
	void Update () {
        
        //increase level
		if (scores > level * level * 20 + 20 * level && SceneManager.GetActiveScene().name.Equals("Game"))
        {
            level++;
            ++scores;
            Save();
            levelChanger.FadeToLevel("Boss" + (level - 1));
        }

        //increase lifes
        if(scores % 20 == 0)
        {
            scores++;
        }

        playerLifes = playerController.GetLifes();
        scoreText.text = "" + scores;
        livesText.text = "" + playerLifes;
        
        //if player is dead
        if(playerLifes <= 0)
        {
            SaveHighScore();
            PlayerPrefs.SetInt("HighScore", scores);
            SceneManager.LoadScene("Menu");
        }
	}

    public void IncreaseScores()
    {
        scores += level;
    }

    public int GetLevel()
    {
        return level;
    }
    
    public void Save()
    {
        PlayerPrefs.SetInt("Level", level);
        PlayerPrefs.SetInt("Scores", scores);
        PlayerPrefs.SetFloat("Shoot", playerController.GetTimeBetweenShoots());
        PlayerPrefs.SetInt("BulletsState", playerController.GetBullets());
    }

    public void SaveHighScore()
    {
        if(scores > highScore[0])
        {
            highScore[0] = scores;
        }
        Array.Sort(highScore);
        for(int i = 0; i < 10; i++)
        {
            PlayerPrefs.SetInt("HighScore" + i, highScore[i]);
        }
    }

    public void LoadHighScore()
    {
        for (int i = 0; i < 10; i++)
        {
            highScore[i] = PlayerPrefs.GetInt("HighScore" + i, 0);
        }
    }
}
