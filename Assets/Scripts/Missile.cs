﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour {

    private Rigidbody2D rig;
    private GameController gameController;
    private GameObject particle;
    private GameObject[] meteorCollection;
    private GameObject[] enemyCollection;
    private GameObject[] enemyCollider;

    void Start () {

        particle = Resources.Load("MissileParticle") as GameObject;
        gameController = FindObjectOfType<GameController>();
        rig = GetComponent<Rigidbody2D>();
        rig.velocity = new Vector3(-2, 3, 0);
	}
	
	void Update () {
		if(transform.position.y > 0)
        {
            CollectMeteors();
            for(int i = 0; i < meteorCollection.Length; i++)
            {
                gameController.IncreaseScores();
                Destroy(meteorCollection[i]);
            }
            for (int i = 0; i < enemyCollection.Length; i++)
            {
                gameController.IncreaseScores();
                Destroy(enemyCollection[i]);
            }
            Instantiate(particle, transform.position, transform.rotation);
            Destroy(gameObject);
        }
	}

    public void CollectMeteors()
    {
        meteorCollection = GameObject.FindGameObjectsWithTag("meteor");
        enemyCollection = GameObject.FindGameObjectsWithTag("enemy");
    }

}
