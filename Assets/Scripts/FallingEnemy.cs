﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingEnemy : MonoBehaviour
{

    private float padding = 0.5f;
    private float xmin;
    private float xmax;
    private float speed = 0.05f;
    private int direction = 1;
    private float tempTime = 0;
    private float timer = 0f;
    private float timeBetweenShots = 3f;

    private int lifes;

    private Bullet bullet;
    private EnemySpawn enemySpawn;
    private GameController gameController;
    private GameObject bonus;
    private GameObject particle;

    void Start()
    {
        particle = Resources.Load("Destroy") as GameObject;
        bonus = Resources.Load("Bonus") as GameObject;
        enemySpawn = FindObjectOfType<EnemySpawn>();
        gameController = FindObjectOfType<GameController>();
        bullet = GetComponent<Bullet>();
        float distance = transform.position.z - Camera.main.transform.position.z;
        Vector3 leftmost = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance));
        Vector3 rightmost = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, distance));
        xmin = leftmost.x + padding;
        xmax = rightmost.x - padding;

        lifes = gameController.GetLevel();
    }

    void Update()
    {
        tempTime += Time.deltaTime;
        if (tempTime > timeBetweenShots)
        {
            {
                bullet.Fire(new Vector3(0, -1, 0), 3);
                tempTime = 0;
            }
        }

        if (lifes <= 0)
        {
            Instantiate(particle, transform.position, transform.rotation);
            float bonusX = Random.Range(0, 100);
            if (bonusX <= 80)
            {
                enemySpawn.SpawnBonus(bonus, gameObject.transform.position);
            }
            Destroy(gameObject);
        }
    }

    private void FixedUpdate()
    {
            transform.Translate(0, speed, 0);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "bulletPlayer")
        {
            gameController.IncreaseScores();
            Destroy(collision.gameObject);
            lifes--;
        }
    }
}
