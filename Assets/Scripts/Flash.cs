﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class Flash : MonoBehaviour {

    public CanvasGroup myCanvas;

    private bool isFlash = false;
	
	void Update () {
		
        if (isFlash)
        {
            myCanvas.alpha -= 5 * Time.deltaTime;
            if(myCanvas.alpha <= 0)
            {
                myCanvas.alpha = 0;
                isFlash = false;
            }
        }
	}

    public void FlashBang()
    {
        isFlash = true;
        myCanvas.alpha = 1;
    }
}
