# Gary Cosmos

You are Gary now. Last man standing in a little spaceship. You will be faced by enemies and powerful bosses. Do not forget about collecting boosts to become stronger.

## Informations
It is space shooter game for Android, made in Unity3d.

[![Gary Cosmos](https://static.itch.io/images/store_badges/google.png)](https://play.google.com/store/apps/details?id=com.Company.SpaceTemp "Gary Cosmos")

## YouTube
[![Gary Cosmos](https://i.imgur.com/LjKzVQX.png)](http://www.youtube.com/watch?v=bUY_gIOUSu8 "Gary Cosmos")

<iframe width="347" height="195" src="https://www.youtube.com/embed/bUY_gIOUSu8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Screenshots
<img src="https://img.itch.zone/aW1hZ2UvMzE5OTM4LzE1Nzg4MDAucG5n/original/vJjGVd.png" width=200 height=300>
<img src="https://img.itch.zone/aW1hZ2UvMzE5OTM4LzE1Nzg4MDEucG5n/original/1MCCFi.png" width=200 height=300>

## My social media
[Itch.io](https://tomeonex.itch.io/)